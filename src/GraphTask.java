import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (3, 2);
      //long timer = System.currentTimeMillis();
      //multiplier(2, g);
      //System.out.println("Time: " + (System.currentTimeMillis() - timer) + "ms");
      Graph firstGraph = new Graph ("Main Graph");
      Vertex vertA = new Vertex ("A");
      Vertex vertB = new Vertex ("B");
      Vertex vertC = new Vertex ("C");
      Vertex vertD = new Vertex ("D");
      firstGraph.vertexis.add(vertA);
      firstGraph.vertexis.add(vertB);
      firstGraph.vertexis.add(vertC);
      firstGraph.vertexis.add(vertD);
      vertA.info = 0;
      vertB.info = 1;
      vertC.info = 2;
      vertD.info = 3;
      firstGraph.first = vertA;
      firstGraph.info++;
      vertA.next = vertB;
      firstGraph.info++;
      vertB.next = vertC;
      firstGraph.info++;
      vertC.next = vertD;
      firstGraph.info++;
      vertD.next = null;
      Arc arcAB = new Arc ("AB");
      Arc arcAC = new Arc ("AC");
      Arc arcAD = new Arc ("AD");
      Arc arcBC = new Arc ("BC");
      Arc arcCD = new Arc ("CD");
      arcAB.target = vertB;
      arcAC.target = vertC;
      arcAD.target = vertD;
      arcBC.target = vertC;
      arcCD.target = vertD;
      vertA.first = arcAB;
      vertB.first = arcBC;
      vertC.first = arcCD;
      arcAB.next = arcAC;
      arcAC.next = arcAD;
      arcAD.next = null;
      arcBC.next = null;
      arcCD.next = null;
      int exponent = 3;
      System.out.println("\nWas given a Graph that: " + firstGraph + "\nwhat was muliplied by: " + exponent + "\nand the result was:");
      System.out.println(multiplier(exponent, firstGraph));
      // TODO!!! Your experiments here
   }

   /** Making a new Graph multiplied by exponent.
    * @autor Eerik Kodasma
    * @param exponent - how many time to multiply to get the endpoints.
    * @param oldGraph - to get info from the initial Graph.
    * @return new Graph that has been multiplied.
    * */
   public Graph multiplier(int exponent, Graph oldGraph){
      Graph n_graph = new Graph("New Graph");
      if (!oldGraph.vertexis.isEmpty()) {
         Vertex currentVertex = null;
         for (int i = 0; i < oldGraph.vertexis.size(); i++) {
            if (i == 0) {
               Vertex firstVer = new Vertex(oldGraph.vertexis.get(0).id);
               n_graph.first = firstVer;
               n_graph.vertexis.add(firstVer);
               currentVertex = firstVer;
            }else {
               Vertex nextVer = new Vertex(oldGraph.vertexis.get(i).id);
               currentVertex.next = nextVer;
               n_graph.vertexis.add(nextVer);
               currentVertex = nextVer;
            }
            currentVertex.first = null;
         }
      }
      if (exponent < 0){
         throw new IllegalArgumentException ("Exponent needs to be greater than 0");
      } else if (exponent == 0) {
         return n_graph;
      } else if (exponent == 1) {
         oldGraph.id = n_graph.id;
         return oldGraph;
      }
      List<List<Integer>> listOfEnds = findingEndPoints(exponent, oldGraph);
      int counter = 0;
      for (List<Integer> endpointsIndex : listOfEnds) {
         Arc currentArc = null;
         if (!endpointsIndex.isEmpty()) {
            for (int num : endpointsIndex) {
               String id = n_graph.vertexis.get(counter).id + n_graph.vertexis.get(num).id;
               Arc arc = new Arc(id, n_graph.vertexis.get(num), null);
               if (endpointsIndex.indexOf(num) == 0) {
                  n_graph.vertexis.get(counter).first = arc;
               }
               if (endpointsIndex.indexOf(num) != 0) {
                  assert currentArc != null;
                  currentArc.next = arc;
               }
               currentArc = arc;
            }
         }
         counter++;
      }
      return n_graph;
   }

   /** Finding Vertexes that are connected to after multiplication.
    * @author  Eerik Kodasma
    * @param exponent - how many time to multiply to get the endpoints.
    * @param oldGraph - to get info from the initial Graph.
    * @return List of endpoints inside of list.
    */
   public List<List<Integer>> findingEndPoints(int exponent, Graph oldGraph) {
      Vertex current_vert = oldGraph.first; // a
      List<List<Integer>> vertexList = new ArrayList<>();
      for (int j = 0; j < oldGraph.info; j++) {
         int howmanytimes = exponent;
         List<Integer> newVertexList = new ArrayList<>();
         boolean firstreading = true;
         int matrixnum = current_vert.info;
         int[][] tempmatrix = oldGraph.createAdjMatrix();
         while (howmanytimes != 0) {
            if (firstreading) {
               for (int i = 0; i < oldGraph.info; i++) {
                  if (tempmatrix[matrixnum][i] == 1 && matrixnum != i) {
                     newVertexList.add(i);
                     tempmatrix[matrixnum][i] = 0;
                     tempmatrix[i][matrixnum] = 0;
                  }
               }
               firstreading = false;
            }else {
               List<Integer> currentVertexList = new ArrayList<>();
               if (!newVertexList.isEmpty()) {
                  for (int k = 0; k < newVertexList.size(); k++) {
                     for (int i = 0; i < oldGraph.info; i++) {
                        if (tempmatrix[newVertexList.get(k)][i] == 1 && j != i) {
                           if (!currentVertexList.contains(i)) {
                              currentVertexList.add(i);
                              //tempmatrix[newVertexList.get(k)][i] = 0;
                              tempmatrix[i][newVertexList.get(k)] = 0;
                           }
                        }
                     }
                  }
               }
               newVertexList = currentVertexList;
            }
            howmanytimes--;
         }
         vertexList.add(newVertexList);
         if (j != oldGraph.info - 1) {
            current_vert = current_vert.next;
         }
      }
      return vertexList;
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public boolean hasnextVert() {
         return this.next != null;
      }
      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public boolean hasnextArc() {
         return this.next != null;
      }
      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private List<Vertex> vertexis= new ArrayList<>();
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
         for (int i = 1; i <= varray.length; i++) {
            this.vertexis.add(varray[varray.length - i]);
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
   }

} 

